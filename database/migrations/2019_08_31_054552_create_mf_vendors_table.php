<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_vendors', function (Blueprint $table) {
            $table->bigIncrements('vid');
            $table->string('vendor_shopname');
            $table->string('vendor_address');
            $table->string('vendor_city');
            $table->string('vendor_state');
            $table->unsignedBigInteger("cid")->comment('categories_id');
            $table->foreign('cid')->references('cid')->on('mf_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_vendors');
    }
}
