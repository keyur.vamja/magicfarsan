<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_users', function (Blueprint $table) {
            $table->bigIncrements('uid');
            $table->string('uname',255);
            $table->string('uemail');
            $table->string('umobileno');
            $table->string('upassword',40);
            $table->unsignedBigInteger("ucityid");
            $table->foreign('ucityid')->references('cityid')->on('mf_cities');
            $table->unsignedBigInteger("ustateid");
            $table->foreign('ustateid')->references('sid')->on('mf_states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_users');
    }
}
