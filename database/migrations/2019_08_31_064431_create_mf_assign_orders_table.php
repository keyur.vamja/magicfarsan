<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfAssignOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_assign_orders', function (Blueprint $table) {
            $table->bigIncrements('aoid');
            $table->unsignedBigInteger("oid");
            $table->foreign('oid')->references('oid')->on('mf_orders');
            $table->unsignedBigInteger("suid")->comment('mf_systemusers_id => delivery_boys');
            $table->foreign('suid')->references('suid')->on('mf_systemusers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_assign_orders');
    }
}
