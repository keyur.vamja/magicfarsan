<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CteateMfCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_cities', function (Blueprint $table) {
            $table->bigIncrements('cityid');
            $table->string('city_name',255);
            $table->unsignedBigInteger("sid");
            $table->foreign('sid')->references('sid')->on('mf_states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_cities');
    }
}
