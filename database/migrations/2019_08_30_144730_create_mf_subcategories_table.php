<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_subcategories', function (Blueprint $table) {
            $table->bigIncrements('scid');
            $table->unsignedBigInteger("cid");
            $table->foreign('cid')->references('cid')->on('mf_categories');
            $table->string('scname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_subcategories');
    }
}
