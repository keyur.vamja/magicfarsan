<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_orders', function (Blueprint $table) {
            $table->bigIncrements('oid');
            $table->string('order_total_qty');
            $table->string('order_total_price');
            $table->enum('order_payment_type',['COD','Online'])->default('COD');
            $table->string('order_address');
            $table->unsignedBigInteger("uid");
            $table->foreign('uid')->references('uid')->on('mf_users');
            $table->unsignedBigInteger("ocityid");
            $table->foreign('ocityid')->references('cityid')->on('mf_cities');
            $table->unsignedBigInteger("osid");
            $table->foreign('osid')->references('sid')->on('mf_states');
            $table->string('uniqe_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_orders');
    }
}
