<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_products', function (Blueprint $table) {
            $table->bigIncrements('pid');
            $table->string('product_name');
            $table->string('product_desc');
            $table->unsignedBigInteger("product_cid");
            $table->foreign('product_cid')->references('cid')->on('mf_categories');
            $table->unsignedBigInteger("product_suid")->comment('mf_systemusers_id');
            $table->foreign('product_suid')->references('suid')->on('mf_systemusers');
            $table->unsignedBigInteger("product_vid");
            $table->foreign('product_vid')->references('vid')->on('mf_vendors');
            $table->unsignedBigInteger("product_package_size");
            $table->foreign('product_package_size')->references('psid')->on('mf_packing_size');
            $table->unsignedBigInteger("product_scid")->default(0)->comment('mf_subcategories_id');
            $table->foreign('product_scid')->references('scid')->on('mf_subcategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_products');
    }
}
