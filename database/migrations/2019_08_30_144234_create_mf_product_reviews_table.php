    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfProductReviewsTable extends Migration
{                                                                                                                                                                                                                                                                                   
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_product_reviews', function (Blueprint $table) {
            $table->bigIncrements('prid');
            $table->string('feedback');
            $table->string('rank');
            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_product_reviews');
    }
}
