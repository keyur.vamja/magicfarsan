<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfProductOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_product_orders', function (Blueprint $table) {
            $table->bigIncrements('poid');
            $table->unsignedBigInteger("oid");
            $table->foreign('oid')->references('oid')->on('mf_orders');
            $table->unsignedBigInteger("pid");
            $table->foreign('pid')->references('pid')->on('mf_products');
            $table->unsignedBigInteger("psid")->comment('mf_packing_size_id');
            $table->foreign('psid')->references('psid')->on('mf_packing_size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_product_orders');
    }
}
