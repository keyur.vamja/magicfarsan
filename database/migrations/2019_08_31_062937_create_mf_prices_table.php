<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_prices', function (Blueprint $table) {
            $table->bigIncrements('ptid');
            $table->unsignedBigInteger("pid");
            $table->foreign('pid')->references('pid')->on('mf_products');
            $table->unsignedBigInteger("psid")->comment('mf_packing_size_id');
            $table->foreign('psid')->references('psid')->on('mf_packing_size');
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_prices');
    }
}
