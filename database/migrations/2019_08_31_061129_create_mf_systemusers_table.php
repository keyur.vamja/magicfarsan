<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfSystemusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_systemusers', function (Blueprint $table) {
            $table->bigIncrements('suid');
            $table->string('suname');
            $table->string('suemail');
            $table->string('sumobileno');
            $table->string('supassword');
            $table->unsignedBigInteger("sucityid");
            $table->foreign('sucityid')->references('cityid')->on('mf_cities');
            $table->unsignedBigInteger("susid");
            $table->foreign('susid')->references('sid')->on('mf_states');
            $table->enum('surole',['sub_admin','delivery_boy'])->default('delivery_boy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_systemusers');
    }
}
