<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfUserAdressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mf_user_adress', function (Blueprint $table) {
            $table->bigIncrements('uaid');
            $table->unsignedBigInteger("uid");
            $table->foreign('uid')->references('uid')->on('mf_users');
            $table->string('user_address');
            $table->unsignedBigInteger("ucityid");
            $table->foreign('ucityid')->references('cityid')->on('mf_cities');
            $table->unsignedBigInteger("ustateid");
            $table->foreign('ustateid')->references('sid')->on('mf_states');
            $table->enum('user_address_type',['Home','Work','Other'])->default('Home');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mf_user_adress');
    }
}
